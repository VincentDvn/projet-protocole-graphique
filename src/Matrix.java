import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Matrix {
    private int[][] matrix;

    public void initMatrix(ArrayList<int[]> listOfBin) {
        int nbChar = listOfBin.size()-2;
        int nbCol = 9+nbChar*3;
        matrix = new int[7][nbCol];

        for (int[] row : matrix)
            Arrays.fill(row, 0);
        setDetection(listOfBin.get(0),nbCol);
        listOfBin.remove(nbChar+1);
        listOfBin.remove(0);

        setChar(listOfBin);
    }

    private void setDetection(int[] blocDetection, int nbCol) {
        int j = -1;
        for (int i = 0; i < blocDetection.length; i++) {
            if(i%4==0)
                j++;
            matrix[j][i%4]=blocDetection[i];
            matrix[j][nbCol-4+i%4]=blocDetection[i];
        }
    }

    private void setChar(ArrayList<int[]> chars){
        int pas=5;
        for (int[] binchar : chars) {
            int j = -1;
            for (int i = 0; i < binchar.length; i++) {
                if(i%2==0)
                    j++;
                matrix[j+1][i%2+pas]=binchar[i];
            }
            pas+=3;
        }
    }

    public int[][] getMatrix(){
        return matrix;
    }

    public void matrixToImage(String name) throws IOException {
        int height = matrix[0].length;
        int width = matrix.length;
        BufferedImage im = new BufferedImage(height, width, BufferedImage.TYPE_BYTE_BINARY);

        int white = (255 << 16) | (255 << 8) | 255;
        int black = 0;

        for (int y = 0; y < width; y++)
            for (int x = 0; x < height; x++)
                im.setRGB(x, y, matrix[y][x] == 1 ? black : white);

        File outputfile = new File(name+".png");
        ImageIO.write(im, "png", outputfile);
    }
}

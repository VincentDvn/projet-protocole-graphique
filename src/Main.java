import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.ArrayList;

public class Main
{

    static void print(int tab[])
    {
        for(int i = 0; i < tab.length; i++)
        {
            System.out.print(tab[i]);
            /*
            if(i%2 == 0)
            {
                System.out.println();
            }
            */
        }
        System.out.println();
    }

    public static String toBinary(String str)
    {

        byte[] buff = str.getBytes(StandardCharsets.UTF_8);
        StringBuilder result = new StringBuilder();
        for(int i = 0; i < buff.length; i++)
        {
            int ch = (int) buff[i];
            String binary = Integer.toBinaryString(ch);
            result.append(("00000000" + binary).substring(binary.length()));
            result.append(',');
        }
        return result.toString();
    }

    static int[] calculation(int[] tab)
    {
        //int r = 4;  4 bit de controle

        for(int i = 0; i < 4; i++)
        {
            int x = (int)Math.pow(2, i);
            for(int j = 1; j < tab.length; j++)
            {
                if(((j >> i) & 1) == 1)
                {
                    if(x != j)
                    {
                        tab[x] = tab[x] ^ tab[j];
                    }
                }
            }
        }
        return tab;
    }

    static int[] generateCode(String str)
    {
        int M = 8;  // Nombre de bit d'info
        int r = 4;  // Nombre de bit de controle

        int[] tab = new int[r + M + 1];
        int j = 0;

        for(int i = 1; i < tab.length; i++)
        {
            if((Math.ceil(Math.log(i) / Math.log(2)) - Math.floor(Math.log(i) / Math.log(2))) == 0)
            {
                tab[i] = 0;
                /*
                    Si i == 2^n for n in (0, 1, 2, ...)
                        Alors tab[i] = 0
                        BitDeControle[i] = 0

                */
            }
            else
            {
                tab[i] = (int)(str.charAt(j) - '0');
                j++;
            }
        }
        return tab;
    }

    static int[] generateDetection(String version, String taille)
    {
        int[] blocDetection = new int[7*4];

        while(version.length() < 4)
        {
            version = "0" + version;
        }

        while(taille.length() < 8)
        {
            taille = "0" + taille;
        }

        for(int i = 0; i < 20; i++)
        {
            if(i < 4)
            {
                blocDetection[i] = 1;
            }
            if(i >= 4 && i < 8)
            {
                if(i%2 == 0)
                {
                    blocDetection[i] = 1;
                }
                else
                {
                    blocDetection[i] = 0;
                }
            }
            if (i >= 8 && i < 12)
            {
                blocDetection[i] = Character.getNumericValue(version.charAt(i-8));
            }
            if(i >= 12)
            {
                blocDetection[i] = Character.getNumericValue(taille.charAt(i-12));
            }
        }

        return blocDetection;
    }

    public static int[] reverse(int[] tab){
        int[] newval = tab.clone();
        for(int i=0; i<tab.length-1;i++){
            newval[i]=tab[tab.length-1-i];
        }
        newval = Arrays.copyOf(newval, newval.length-1);
        return newval;
    }

    public static void saveImage(int[][] mat) throws IOException {
        int height = mat[0].length;
        int width = mat.length;
        BufferedImage im = new BufferedImage(height, width, BufferedImage.TYPE_BYTE_BINARY);

        int white = (255 << 16) | (255 << 8) | 255;
        int black = 0;

        for (int y = 0; y < width; y++)
            for (int x = 0; x < height; x++)
                im.setRGB(x, y, mat[y][x] == 1 ? black : white);

        File outputfile = new File("code.png");
        ImageIO.write(im, "png", outputfile);
    }

    public static void main(String[] args) throws IOException
    {
        Matrix m = new Matrix();
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        ArrayList<int[]> listBinaire = new ArrayList<int[]>();

        System.out.println("Mot a encoder : ");
        String str = reader.readLine();


        int[] blocDetection = generateDetection(Integer.toBinaryString(1), Integer.toBinaryString(str.length()));       // Ici pour une version 1
        // On ne développera pas les autres versions
        listBinaire.add(blocDetection);

        String[] bin = toBinary(str).split(",");

        StringBuilder sb = new StringBuilder();
        StringBuilder sb2 = new StringBuilder();
        for(String a : bin)
        {
            sb.replace(0,8,a);
            int[] tab = generateCode(sb.reverse().toString());
            tab = calculation(tab);
            listBinaire.add(reverse(tab));
        }

        listBinaire.add(blocDetection);

        m.initMatrix(listBinaire);
        m.matrixToImage(str);
    }

}